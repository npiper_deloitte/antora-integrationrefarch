# Deloitte Cloud Engineering - Integration Reference Architecture

## Concept

For a project where intelligent, developer friendly documentation is required 

This approach is a road test of using 'code' driven samples that is close to the developer audience and in the tools they prefer to improve the collaboration between developers and architects.

## Topic

This is an Arc42 structured approach to a documentation reference architecture

## Setup



## Dockerfile

*TO DO : Make it easy for new starters to build, display this content*

## Sharepoint Reference

[Deloitte Hybrid Integration Reference Architecture v0.3](https://www.dumyuy.com)

## Technology

### Documentation 


This document is a prototype format created in Asciidoc and using the Antora documentation framework.

It is contained in folder `modules/ROOT/pages`

It has the goal of being consumable in multiple formats, hyperlinked
version controled and being accessible to multiple parties.

This means it could be possible to have in Github close to developers
as well as being available in HTML, PDF or Confluence section.

It will use the following tools to create / publish: 

 * [antora](https://antora.org/)
 * [asciidoctor](https://asciidoctor.org/)
 * [asciidoc writers guide](https://asciidoctor.org/docs/asciidoc-writers-guide/#links-and-images) 
 * [draw.io](https://app.diagrams.net)
 * [asciidoc-confluence-publisher-maven-plugin](https://confluence-publisher.atlassian.net/wiki)
 * [asciidoctor-maven-plugin](https://asciidoctor.org/docs/asciidoctor-maven-plugin/)
 * [font awesome (Asciidoc has free v4.X)](https://fontawesome.com/v4.7/icons/)
 * [plantUML](https://plantuml.com/)
 * [plantUML Maven Plugin](https://github.com/jeluard/maven-plantuml-plugin)
 * [FontAwesome v4.7 - Included with asciidoc icon](https://fontawesome.com/v4.7/icons/)


### Research Options

 * Local Dockerfile with all pre-installed pre-reqs?
 * Maven Build - https://abelsromero.github.io/blog/2020/antora-with-maven.html
 * Gradle - https://gitlab.com/antora/antora-gradle-plugin , https://github.com/rwinch/antora-gradle

### What you may need on your Dev environment

 * git
 * antora
 * asciidoctor
 * node.js / npm
 * Java / Maven / gradle
 * graphviz

### Editors

 * IntelliJ - Antora plugin
 * Visual Studio code - Asciidoc, Plantuml preview editor

## Editing the content

### Pages

### Language References

How to write 
 * Arc42
 * C4
 * PlantUML
 * Asciidoc
 * Markdown
 * FontAwesome v4.7

### Diagrams and Images

_Plantuml files .puml, draw.io editor and libraries_

### Icons

_Font awesome support in Asciidoc_

<i class="icon_file"/>